const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/hellojson', (req, res) => {
  console.log(req)
  res.json({
    id: 2000,
    name: 'Iphone'
  })
})

app.get('/hellome', (req, res) => {
  res.send('Hello Me!')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
